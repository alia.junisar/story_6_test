from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.utils import timezone
from .views import index
from .models import Status
from .forms import FormStatus
from .apps import Story6Config

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time

class Story6UnitTest(TestCase):

    def test_story_6_url_is_exist(self):
    	response = Client().get('/')
    	self.assertEqual(response.status_code, 200)
    
    def test_story6_using_index_func(self):
    	found = resolve('/')
    	self.assertEqual(found.func, index)
    
    def test_story6_using_index_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'index.html')
    
    def test_form_can_be_used(self):
    	isi = Client().post('/', status = {'status' : 'Saya senang'})
    	self.assertFalse(Status.objects.filter(status='Saya senang'))
    
    def test_form_validation_for_blank_items(self):
    	form = FormStatus(data={'status': ''})
    	self.assertFalse(form.is_valid())
    	self.assertEqual(
    	    form.errors['status'],
    	    ["This field is required."]
    	)

class Story6FunctionalTest(TestCase):
    
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument("--disable-dev-shm-usage")
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(Story6FunctionalTest, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(Story6FunctionalTest, self).tearDown()

    def test_input_status(self):
        selenium = self.selenium
        # Opening the link we want to test
        selenium.get('http://localhost:8000')
        time.sleep(5)

        # find the form element
        status = selenium.find_element_by_id('id_status')
        submit = selenium.find_element_by_id('submit-button')

        # Fill the form with data
        status.send_keys('Coba Coba')

        # submitting the form
        submit.send_keys(Keys.RETURN)
