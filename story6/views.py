from django.shortcuts import render
from django.shortcuts import redirect
from .forms import FormStatus
from .models import Status

def index(request):

    form_status = FormStatus(request.POST or None)
    if request.method == "POST":

        if form_status.is_valid():
            isi_status = form_status.cleaned_data['status']
            status = Status(status = isi_status)
            status.save()

            return redirect('/')

    else:
        form_status = FormStatus()
            
    semua_status = Status.objects.all()
	
    contexts = {'posts': semua_status, 'form_status': form_status}
    
    return render(request, 'index.html', contexts)
